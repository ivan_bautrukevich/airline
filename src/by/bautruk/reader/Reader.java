package by.bautruk.reader;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author ivan.bautruvkevich
 *         27.11.2015 18:07.
 */
public class Reader {

    static Logger logger = Logger.getLogger(Reader.class);

    public static ArrayList<String> read(String fileName) {

        ArrayList<String> inData = new ArrayList<String>();

        try (Scanner in = new Scanner(new File(fileName))) {
            while (in.hasNext()) {
                inData.add(in.nextLine());
            }
        } catch (FileNotFoundException e) {
            logger.error(fileName + " - File is no exist");
        }
        return inData;
    }
}

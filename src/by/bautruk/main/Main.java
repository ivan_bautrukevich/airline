package by.bautruk.main;

import by.bautruk.entity.Airline;
import by.bautruk.entity.CalculateType;
import by.bautruk.entity.SortType;
import by.bautruk.logic.AirlineManager;
import by.bautruk.reader.Reader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * @author ivan.bautruvkevich
 *         24.11.2015 20:28.
 */
public class Main {

    static {
        new DOMConfigurator().doConfigure("xml/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        String name = "data/plane.txt";

        AirlineManager am = new AirlineManager();
        Airline bestAirline = new Airline();
        am.parse(bestAirline, Reader.read(name));
        am.Calculate(bestAirline, CalculateType.CAPACITY);
        am.SortAirline(bestAirline, SortType.FLYDISTANCE);
        am.SeachAirplane(bestAirline, 100, 200);
    }
}
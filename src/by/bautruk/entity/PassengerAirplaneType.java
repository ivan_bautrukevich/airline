package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 20:13.
 */
public enum PassengerAirplaneType {
    WIDEBODY,
    NARROWBODY,
    REGIONAL
}

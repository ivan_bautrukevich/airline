package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 15:30.
 */
public class FighterAirplaneData extends AirplaneData {

    private int firePower;

    public FighterAirplaneData(int id, String name, int flyDistance, int capacity, int carrying, int consumption, int firePower) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.firePower = firePower;
    }

    public int getFirePower() {
        return firePower;
    }
}

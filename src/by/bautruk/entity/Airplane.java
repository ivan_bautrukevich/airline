package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 19:41.
 */
public abstract class Airplane {


    private int id;
    private String name;
    private int flyDistance;
    private int capacity;
    private int carrying;
    private int consumption;

    public Airplane(int id, String name, int flyDistance, int capacity, int carrying, int consumption) {
        this.id = id;
        this.name = name;
        this.flyDistance = flyDistance;
        this.capacity = capacity;
        this.carrying = carrying;
        this.consumption = consumption;
    }

    public Airplane(AirplaneData airplaneData) {
        this.id = airplaneData.getId();
        this.name = airplaneData.getName();
        this.flyDistance = airplaneData.getFlyDistance();
        this.capacity = airplaneData.getCapacity();
        this.carrying = airplaneData.getCarrying();
        this.consumption = airplaneData.getConsumption();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlyDistance() {
        return flyDistance;
    }

    public void setFlyDistance(int flyDistance) {
        this.flyDistance = flyDistance;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCarrying() {
        return carrying;
    }

    public void setCarrying(int carrying) {
        this.carrying = carrying;
    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Airplane airplane = (Airplane) o;

        if (id != airplane.id) return false;
        if (flyDistance != airplane.flyDistance) return false;
        if (capacity != airplane.capacity) return false;
        if (carrying != airplane.carrying) return false;
        if (consumption != airplane.consumption) return false;
        return !(name != null ? !name.equals(airplane.name) : airplane.name != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + flyDistance;
        result = 31 * result + capacity;
        result = 31 * result + carrying;
        result = 31 * result + consumption;
        return result;
    }
}

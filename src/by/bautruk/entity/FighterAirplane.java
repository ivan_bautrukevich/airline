package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 19:58.
 */
public class FighterAirplane extends Airplane {

    private int firePower;

    public FighterAirplane(int id, String name, int flyDistance, int capacity, int carrying, int consumption, int firePower) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.firePower = firePower;
    }

    public FighterAirplane(FighterAirplaneData fighterData) {
        super(fighterData);
        this.firePower = fighterData.getFirePower();
    }

    public int getFirePower() {
        return firePower;
    }

    public void setFirePower(int firePower) {
        this.firePower = firePower;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FighterAirplane that = (FighterAirplane) o;

        return firePower == that.firePower;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + firePower;
        return result;
    }

    @Override
    public String toString() {
        return "FighterAirplane{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", flyDistance=" + getFlyDistance() +
                ", capacity=" + getCapacity() +
                ", carrying=" + getCarrying() +
                ", consumption=" + getConsumption() +
                ", firePower" + getFirePower() +
                '}';
    }
}

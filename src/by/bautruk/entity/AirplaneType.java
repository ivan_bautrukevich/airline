package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 14:29.
 */
public enum AirplaneType {
    CARGO,
    FIGHTER,
    PASSENGER
}

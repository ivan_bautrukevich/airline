package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 15:05.
 */
public class CargoAirplaneData extends AirplaneData {

    private int volumeCargo;

    public CargoAirplaneData(int id, String name, int flyDistance, int capacity, int carrying, int consumption, int volumeCargo) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.volumeCargo = volumeCargo;
    }

    public int getVolumeCargo() {
        return volumeCargo;
    }
}

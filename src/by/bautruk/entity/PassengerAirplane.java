package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 19:57.
 */
public class PassengerAirplane extends Airplane {

    private PassengerAirplaneType type;

    public PassengerAirplane(int id, String name, int flyDistance, int capacity, int carrying, int consumption, PassengerAirplaneType type) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.type = type;
    }

    public PassengerAirplane(PassengerAirplaneData passengerData) {
        super(passengerData);
        this.type = passengerData.getType();
    }

    public PassengerAirplaneType getType() {
        return type;
    }

    public void setType(PassengerAirplaneType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PassengerAirplane that = (PassengerAirplane) o;

        return type == that.type;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PassengerAirplane{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", flyDistance=" + getFlyDistance() +
                ", capacity=" + getCapacity() +
                ", carrying=" + getCarrying() +
                ", consumption=" + getConsumption() +
                ", type=" + getType() +
                '}';
    }
}

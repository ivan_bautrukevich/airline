package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         01.12.2015 19:58.
 */
public enum SortType {
    CAPACITY,
    CARRYING,
    FLYDISTANCE
}

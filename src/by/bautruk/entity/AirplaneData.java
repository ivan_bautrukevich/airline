package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 15:03.
 */
public abstract class AirplaneData {

    private int id;
    private String name;
    private int flyDistance;
    private int capacity;
    private int carrying;
    private int consumption;

    public AirplaneData(int id, String name, int flyDistance, int capacity, int carrying, int consumption) {
        this.id = id;
        this.name = name;
        this.flyDistance = flyDistance;
        this.capacity = capacity;
        this.carrying = carrying;
        this.consumption = consumption;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFlyDistance() {
        return flyDistance;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getCarrying() {
        return carrying;
    }

    public int getConsumption() {
        return consumption;
    }
}

package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         26.11.2015 23:11.
 */
public enum CalculateType {
    CAPACITY,
    CARRYING
}

package by.bautruk.entity;

import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 20:18.
 */
public class Airline {
    private ArrayList<Airplane> airline = new ArrayList<Airplane>();

    public Airline() {
        airline = new ArrayList<>();
    }

    public ArrayList<Airplane> getAirline() {
        return airline;
    }

    public Airplane get(int index) {
        return airline.get(index);
    }

    public int size() {
        return airline.size();
    }

    public boolean add(Airplane airplane) {
        return airline.add(airplane);
    }
}


package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 15:34.
 */
public class PassengerAirplaneData extends AirplaneData {

    private PassengerAirplaneType type;

    public PassengerAirplaneData(int id, String name, int flyDistance, int capacity, int carrying, int consumption, PassengerAirplaneType type) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.type = type;
    }

    public PassengerAirplaneType getType() {
        return type;
    }
}

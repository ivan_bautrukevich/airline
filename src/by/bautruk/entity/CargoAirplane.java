package by.bautruk.entity;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 19:56.
 */
public class CargoAirplane extends Airplane {

    private int volumeCargo; // volume of inside space


    public CargoAirplane(int id, String name, int flyDistance, int capacity, int carrying, int consumption, int volumeCargo) {
        super(id, name, flyDistance, capacity, carrying, consumption);
        this.volumeCargo = volumeCargo;
    }

    public CargoAirplane(CargoAirplaneData cargoData) {
        super(cargoData);
        this.volumeCargo = cargoData.getVolumeCargo();
    }

    public void setVolumeCargo(int volumeCargo) {
        this.volumeCargo = volumeCargo;
    }

    public int getVolumeCargo() {

        return volumeCargo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CargoAirplane that = (CargoAirplane) o;

        return volumeCargo == that.volumeCargo;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + volumeCargo;
        return result;
    }

    @Override
    public String toString() {
        return "CargoAirplane{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", flyDistance=" + getFlyDistance() +
                ", capacity=" + getCapacity() +
                ", carrying=" + getCarrying() +
                ", consumption=" + getConsumption() +
                ", volumeCargo=" + getVolumeCargo() +
                '}';
    }
}

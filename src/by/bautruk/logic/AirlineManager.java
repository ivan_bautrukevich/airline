package by.bautruk.logic;

import by.bautruk.entity.Airline;
import by.bautruk.entity.Airplane;
import by.bautruk.entity.CalculateType;
import by.bautruk.entity.SortType;
import by.bautruk.service.Calculate;
import by.bautruk.service.SearchAirplaneConsumption;
import by.bautruk.service.SortAirline;

import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         25.11.2015 0:20.
 */
public class AirlineManager {

 //   private static final AirplaneFactory factory = new AirplaneFactory();

    public Airline parse(Airline airline, ArrayList<String> inData){
        Airline bestAirline = AirlineCreator.parser(airline, inData);
        return bestAirline;
    }


    public int Calculate(Airline airline, CalculateType calculateType) {
        int sum = Calculate.calculateTotal(airline, calculateType);
        return sum;
    }

    public void SortAirline(Airline airline, SortType sortIndex){
        SortAirline.sortFlyDistance(airline, sortIndex);
    }

    public ArrayList<Airplane> SeachAirplane(Airline airline, int consumptionMin, int consumptionMax){
        ArrayList<Airplane> searchAirplane = SearchAirplaneConsumption.searchConsumption(airline, consumptionMin, consumptionMax);
        return searchAirplane;
    }



}
/*
    public Airline getNewAirline() {
        return new Airline();
    }

    public Airline addCargoAirplane(Airline airline, int id, String name, int flyDistance, int capacity, int carrying, int consumption, int volumeCargo) {

        Airplane airplane = factory.getCargoAirplane(airline, id, name, flyDistance, capacity, carrying, consumption, volumeCargo);
        airline.getAirline().add(airplane);
        return airline;
    }

    public Airline addFighterAirplane(Airline airline, int id, String name, int flyDistance, int capacity, int carrying, int consumption, int firePower) {

        Airplane airplane = factory.getFighterAirplane(airline, id, name, flyDistance, capacity, carrying, consumption, firePower);
        airline.getAirline().add(airplane);
        return airline;
    }

    public Airline addPassengerAirplane(Airline airline, int id, String name, int flyDistance, int capacity, int carrying, int consumption, PassengerAirplaneType type) {

        Airplane airplane = factory.getPassengerAirplane(airline, id, name, flyDistance, capacity, carrying, consumption, type);
        airline.getAirline().add(airplane);
        return airline;
    }
 */
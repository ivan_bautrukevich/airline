package by.bautruk.logic;

import by.bautruk.entity.*;
import by.bautruk.service.validator.ValidatorCargo;
import by.bautruk.service.validator.ValidatorFighter;
import by.bautruk.service.validator.ValidatorPassenger;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 12:55.
 */
public class AirlineCreator {

    static Logger logger = Logger.getLogger(AirlineCreator.class);

    public static Airline parser(Airline bestAirline,ArrayList<String> inData) {

        String[] param = null;

        for (String data : inData) {
            param = data.split(", ");

            switch (AirplaneType.valueOf(param[0])) {
                case CARGO:
                    try {
                        CargoAirplaneData cargoData = new CargoAirplaneData(Integer.parseInt(param[1]), param[2].toString(), Integer.parseInt(param[3]),
                                Integer.parseInt(param[4]), Integer.parseInt(param[5]), Integer.parseInt(param[6]), Integer.parseInt(param[7]));
                        boolean valid = ValidatorCargo.valid(cargoData);
                        if (valid) {
                            bestAirline.add(new CargoAirplane(cargoData));
                        } else {
                            logger.error("Not valid data about Cargo Airplane");
                            throw new IllegalArgumentException();
                        }
                    } catch (NumberFormatException e) {
                        logger.error("Wrong data format fot Cargo Airplane");
                        throw new IllegalArgumentException();
                    }
                    break;
                case FIGHTER:
                    try {
                        FighterAirplaneData fighterData = new FighterAirplaneData(Integer.parseInt(param[1]), param[2].toString(), Integer.parseInt(param[3]),
                                Integer.parseInt(param[4]), Integer.parseInt(param[5]), Integer.parseInt(param[6]), Integer.parseInt(param[7]));
                        boolean valid = ValidatorFighter.valid(fighterData);
                        if (valid) {
                            bestAirline.add(new FighterAirplane(fighterData));
                        } else {
                            logger.error("Not valid data about Fighter Airplane");
                            throw new IllegalArgumentException();
                        }
                    } catch (NumberFormatException e) {
                        logger.error("Wrong data format for Fighter Airplane");
                        throw new IllegalArgumentException();
                    }
                    break;
                case PASSENGER:
                    try {
                        PassengerAirplaneData passengerData = new PassengerAirplaneData(Integer.parseInt(param[1]), param[2].toString(), Integer.parseInt(param[3]),
                                Integer.parseInt(param[4]), Integer.parseInt(param[5]), Integer.parseInt(param[6]), PassengerAirplaneType.valueOf(param[7]));
                        boolean valid = ValidatorPassenger.valid(passengerData);
                        if (valid) {
                            bestAirline.add(new PassengerAirplane(passengerData));
                        } else {
                            logger.error("Not valid data about Passenger Airplane");
                            throw new IllegalArgumentException();
                        }
                    } catch (NumberFormatException e) {
                        logger.error("Wrong data format for Passenger Airplane");
                        throw new IllegalArgumentException();
                    }
                    break;
                default:
                    logger.error("Type " + param[0].toString() + "is illegal airplane");
            }
        }
        logger.info("Airplane is added successfully. Result: "+bestAirline.getAirline());
        return bestAirline;
    }
}

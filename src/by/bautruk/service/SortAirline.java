package by.bautruk.service;

import by.bautruk.entity.Airline;

import by.bautruk.entity.SortType;
import by.bautruk.service.comparator.FlyDistanceComparator;
import org.apache.log4j.Logger;

import java.util.Collections;

/**
 * @author ivan.bautruvkevich
 *         24.11.2015 21:15.
 */
public class SortAirline {

    static Logger logger = Logger.getLogger(SortAirline.class);

    public static void sortFlyDistance(Airline airline, SortType sortIndex) {
        Collections.sort(airline.getAirline(), new FlyDistanceComparator(sortIndex));
        logger.info("Airplane is sorted successfully. Result: " + airline.getAirline());
    }
}

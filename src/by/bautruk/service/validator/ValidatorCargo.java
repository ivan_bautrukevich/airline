package by.bautruk.service.validator;

import by.bautruk.entity.CargoAirplaneData;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 16:04.
 */
public class ValidatorCargo{
    public static boolean valid(CargoAirplaneData data) {
        if (data.getId() > 0 && data.getName() != null && data.getFlyDistance() > 0 &&
                data.getCapacity() > 0 && data.getCarrying() > 0 && data.getConsumption() > 0 &&
                data.getVolumeCargo() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

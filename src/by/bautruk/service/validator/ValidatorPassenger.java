package by.bautruk.service.validator;

import by.bautruk.entity.PassengerAirplaneData;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 16:32.
 */
public class ValidatorPassenger {
    public static boolean valid(PassengerAirplaneData data) {
        if (data.getId() > 0 && data.getName() != null && data.getFlyDistance() > 0 &&
                data.getCapacity() > 0 && data.getCarrying() > 0 && data.getConsumption() > 0 &&
                data.getType() != null) {
            return true;
        } else {
            return false;
        }
    }
}

package by.bautruk.service.validator;

import by.bautruk.entity.FighterAirplaneData;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 16:31.
 */
public class ValidatorFighter {
    public static boolean valid(FighterAirplaneData data) {
        if (data.getId() > 0 && data.getName() != null && data.getFlyDistance() > 0 &&
                data.getCapacity() > 0 && data.getCarrying() > 0 && data.getConsumption() > 0 &&
                data.getFirePower() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

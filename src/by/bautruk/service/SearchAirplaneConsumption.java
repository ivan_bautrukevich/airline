package by.bautruk.service;

import by.bautruk.entity.Airline;
import by.bautruk.entity.Airplane;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * @author ivan.bautruvkevich
 *         29.11.2015 21:03.
 */
public class SearchAirplaneConsumption {

    static Logger logger = Logger.getLogger(SearchAirplaneConsumption.class);

    public static ArrayList<Airplane> searchConsumption(Airline airline, int consumptionMin, int consumptionMax) {
        ArrayList<Airplane> searchAirplane = new ArrayList<>();

        for (int i = 0; i < airline.size(); i++) {
            if (airline.get(i).getConsumption() > consumptionMin && airline.get(i).getConsumption() < consumptionMax) {
                searchAirplane.add(airline.get(i));
            }
        }
        if (searchAirplane.size() == 0) {
            logger.info("Airplane with parameter " + consumptionMin + " and " + consumptionMax + " not exist");
        }
        return searchAirplane;
    }
}

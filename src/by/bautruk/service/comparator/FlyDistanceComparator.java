package by.bautruk.service.comparator;

import by.bautruk.entity.Airplane;
import by.bautruk.entity.SortType;

import java.util.Comparator;

/**
 * @author ivan.bautruvkevich
 *         27.11.2015 0:10.
 */
public class FlyDistanceComparator implements Comparator<Airplane> {

    private SortType sortIndex;

    public FlyDistanceComparator(SortType sortIndex) {
        this.sortIndex = sortIndex;
    }

    public SortType getSortIndex() {
        return sortIndex;
    }

    public final void setSortIndex(SortType sortIndex) {
        if (sortIndex == null) {
            throw new IllegalArgumentException();
        }
        this.sortIndex = sortIndex;
    }

    @Override
    public int compare(Airplane o1, Airplane o2) {
        switch (sortIndex) {
            case FLYDISTANCE:
                int fd1 = o1.getFlyDistance();
                int fd2 = o2.getFlyDistance();
                if (fd1 > fd2) {
                    return 1;
                } else if (fd1 < fd2) {
                    return -1;
                } else {
                    return 0;
                }
            case CAPACITY:
                int cap1 = o1.getCapacity();
                int cap2 = o2.getCapacity();
                if (cap1 > cap2) {
                    return 1;
                } else if (cap1 < cap2) {
                    return -1;
                } else {
                    return 0;
                }
            case CARRYING:
                int car1 = o1.getCapacity();
                int car2 = o2.getCapacity();
                if (car1 > car2) {
                    return 1;
                } else if (car1 < car2) {
                    return -1;
                } else {
                    return 0;
                }
            default:
                throw new EnumConstantNotPresentException(SortType.class, sortIndex.name());
        }
    }
}

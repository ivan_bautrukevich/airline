package by.bautruk.service;

import by.bautruk.entity.Airline;
import by.bautruk.entity.CalculateType;
import org.apache.log4j.Logger;

/**
 * @author ivan.bautruvkevich
 *         26.11.2015 23:12.
 */
public class Calculate {

    static Logger logger = Logger.getLogger(Calculate.class);

    public static int calculateTotal(Airline airline, CalculateType calculateType) {

        int sum = 0;

        switch (calculateType) {
            case CAPACITY:
                for (int i = 0; i < airline.size(); i++) {
                    sum += airline.get(i).getCapacity();
                }
                break;
            case CARRYING:
                for (int i = 0; i < airline.size(); i++) {
                    sum += airline.get(i).getCarrying();
                }
                break;
            default:
                logger.error(calculateType + " - is illegal statement.");
        }
        logger.info("The result of " + calculateType + " is " + sum);

        return sum;
    }
}
